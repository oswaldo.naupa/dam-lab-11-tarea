

import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() {
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  int _conteo = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stateful',
      home: Scaffold(
        backgroundColor: Colors.indigo[100],
        appBar: AppBar(
          actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
          title: Text('Contador', style:TextStyle( fontSize: 28)),
          backgroundColor: Colors.indigo[900],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Número de clicks', style: TextStyle(fontSize: 25)),
              Text('$_conteo', style: TextStyle(fontSize: 50, color: Colors.grey[500]))
            ],
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget> [ 
            FloatingActionButton(
              backgroundColor: Colors.grey[500],
              child: Icon(Icons.refresh),
              onPressed: () => {
                setState((){
                  _conteo = 0;
                })
              },
            ),
            FloatingActionButton(
              backgroundColor: Colors.red[500],
              child: Icon(Icons.remove),
              onPressed: () => {
                if(_conteo>0){
                  setState((){
                    _conteo--;
                  })
                }else{
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) => AlertDialog(
                      title: Text('Mensaje', style: TextStyle(color: Colors.indigo[900],fontWeight: FontWeight.bold)),
                      content: Text('Solo se permiten numeros positivos'),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('Ok'),
                          onPressed: (){
                            Navigator.of(context).pop('Ok');
                          },
                        )
                      ],
                    )
                  )
                }
              },
            ),
            FloatingActionButton(
              backgroundColor: Colors.indigo[900],
              child: Icon(Icons.add),
              onPressed: () => {
                setState((){
                  _conteo++;
                })
              }
            ),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    ); 
  }
}